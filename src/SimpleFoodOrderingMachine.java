import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SimpleFoodOrderingMachine {
    private JPanel root;
    private JLabel topLabel;
    private JButton tempuraButton;
    private JButton ramenButton;
    private JButton udonButton;
    private JTextPane receivedInfo;
    private JButton gyozaButton;
    private JButton yakisobaButton;
    private JButton karaageButton;
    private JButton checkOutButton;
    private JTextPane totalTextPane;

    int total =0;

    void order(String food) {
        int confirmation = JOptionPane.showConfirmDialog(null, "Would you like to order "+food+" ?", "orderComfirmation", JOptionPane.YES_NO_OPTION);
        if (confirmation == 0) {
            String currentItems = receivedInfo.getText();
            receivedInfo.setText(currentItems+food+"\n");
            total+=100;
            totalTextPane.setText(total +"yen");
            JOptionPane.showMessageDialog(null,"Order for "+food+" received.");
        }
    }
    public SimpleFoodOrderingMachine(){
        totalTextPane.setText("0 yen");

        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order ("Tempura");
            }
        });
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen");
            }
        });
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon");
            }
        });
        karaageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order ("Karaage");
            }
        });
        gyozaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order ("Gyoza");
            }
        });
        yakisobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order ("Yakisoba");
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null, "Would you like to checkout?", "checkout Comfirmation", JOptionPane.YES_NO_OPTION);
                if (confirmation == 0) {
                    JOptionPane.showMessageDialog(null,"Thank you ! Total price is "+total+" yen.");
                    total = 0;
                    receivedInfo.setText("");
                    totalTextPane.setText("0 yen");
                }
            }
        });
    }



    public static void main(String[] args) {
        JFrame frame = new JFrame("SimpleFoodOrderingMachine");
        frame.setContentPane(new SimpleFoodOrderingMachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
